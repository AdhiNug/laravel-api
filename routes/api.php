<?php

use App\Http\Controllers\PostController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Post;
use Illuminate\Database\Migrations\Migration;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// CRUD is basically
// 1. get all (GET) /api/posts
// 2. create a post (POST) /api/posts
// 3. get a single (GET) /api/posts/{id}
// 4. update a single (PUT/PATCH) /api/posts/{id}
// 5. delete (DELETE) /api/posts/{id}

// to create a resource (posts) in laravel
// 1. create the database and Migrations
// 2. create model
// 2.5 create a services ? eloquent ORMclear
// 3. create controller to go get info from the database
// 4. return that info  

Route::prefix('posts')->group(function () {
    Route::get('/', 'PostController@index');
    Route::get('/{post}', 'PostController@show');
    Route::post('/', 'PostController@store');
    Route::put('/{post}', 'PostController@update');
    Route::delete('/{post}', 'PostController@destroy');
});

// Route::resource('posts', 'PostController');



Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

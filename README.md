### Aplication SetUp
1. Buat database
2. Buat **.env** file seperti **.env.example**
3. Buat model dan migrasinya<br>
  *<b>ex :</b> `php artisan make:model Post --migration`*
   - 3.a isi method up pada file migration<br>
     ![a relative link](patch/method%20up.jpg)
   - 3.b Lakukan migration
   - 3.c Buka file model Post.php dan isi class modelnya<br>
     ![a relative link](patch/model.jpg)
4. Buat controller<br>
  *<b>ex :</b> `php artisan make:controller PostController --api`*
   - 4.a Buka file controller PostController.php
   - 4.b Panggil model ke dalam controller<br>
     ![a relative link](patch/controller.jpg)
   - 4.c Return setiap data yang diperoleh dari model melalui masing-masing Method yang ada di dalam controller<br>
     ![a relative link](patch/controller_index.jpg)
     ![a relative link](patch/controller_store.jpg)
     ![a relative link](patch/controller_show.jpg)
     ![a relative link](patch/controller_update.jpg)
     ![a relative link](patch/controller_destroy.jpg)
5. Buka file api.php yang ada di dalam folder routes dan buat routes baru di dalamnya, bisa menggunakan type 1 atay type 2<br>
     ![a relative link](patch/routes.jpg)
6. Lakukan pengujian di postman atau insomnia<br>
     ![a relative link](patch/postman.jpg)
7. Selesai.